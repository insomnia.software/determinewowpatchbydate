﻿using System;
using System.Collections.Generic;

namespace WoWExpansionGuide
{
    class Patches
    {
        public string Expansion { get; set; }
        public string Patch { get; set; }

        private readonly List<Patch> patches = new List<Patch>();

        public Patches()
        {
            patches.Add(new Patch { Date = new DateTime(2004, 4, 13), PatchNumber = "0.6" });
            patches.Add(new Patch { Date = new DateTime(2004, 6, 15), PatchNumber = "0.7" });
            patches.Add(new Patch { Date = new DateTime(2004, 7, 7), PatchNumber = "0.8" });
            patches.Add(new Patch { Date = new DateTime(2004, 8, 17), PatchNumber = "0.9" });
            patches.Add(new Patch { Date = new DateTime(2004, 8, 24), PatchNumber = "0.9.1" });
            patches.Add(new Patch { Date = new DateTime(2004, 9, 18), PatchNumber = "0.10" });
            patches.Add(new Patch { Date = new DateTime(2004, 9, 29), PatchNumber = "0.11" });
            patches.Add(new Patch { Date = new DateTime(2004, 10, 11), PatchNumber = "0.12" });
            patches.Add(new Patch { Date = new DateTime(2004, 11, 7), PatchNumber = "1.1.0" });
            patches.Add(new Patch { Date = new DateTime(2004, 11, 17), PatchNumber = "1.1.1" });
            patches.Add(new Patch { Date = new DateTime(2004, 12, 6), PatchNumber = "1.1.2" });
            patches.Add(new Patch { Date = new DateTime(2004, 12, 18), PatchNumber = "1.2.0" });
            patches.Add(new Patch { Date = new DateTime(2004, 12, 21), PatchNumber = "1.2.1" });
            patches.Add(new Patch { Date = new DateTime(2005, 2, 15), PatchNumber = "1.2.2" });
            patches.Add(new Patch { Date = new DateTime(2005, 2, 22), PatchNumber = "1.2.3" });
            patches.Add(new Patch { Date = new DateTime(2005, 2, 23), PatchNumber = "1.2.4" });//patch actually released 22nd same day as 1.2.3
            patches.Add(new Patch { Date = new DateTime(2005, 3, 7), PatchNumber = "1.3.0" });
            patches.Add(new Patch { Date = new DateTime(2005, 3, 22), PatchNumber = "1.3.1" });
            patches.Add(new Patch { Date = new DateTime(2005, 3, 23), PatchNumber = "1.3.2" });//actually 22nd
            patches.Add(new Patch { Date = new DateTime(2005, 4, 5), PatchNumber = "1.4.0" });
            patches.Add(new Patch { Date = new DateTime(2005, 5, 3), PatchNumber = "1.4.1" });//http://www.elitepvpers.com/forum/wow-main-discussions-questions/36941-wow-patch-1-4-1-a.html
            patches.Add(new Patch { Date = new DateTime(2005, 5, 5), PatchNumber = "1.4.2" });//http://www.macobserver.com/tmo/article/Blizzard_Patches_World_of_Warcraft_v.1.4.2
            patches.Add(new Patch { Date = new DateTime(2005, 6, 7), PatchNumber = "1.5.0" });
            patches.Add(new Patch { Date = new DateTime(2005, 6, 14), PatchNumber = "1.5.1" });
            patches.Add(new Patch { Date = new DateTime(2005, 7, 12), PatchNumber = "1.6.0" });
            patches.Add(new Patch { Date = new DateTime(2005, 8, 2), PatchNumber = "1.6.1" });
            patches.Add(new Patch { Date = new DateTime(2005, 9, 13), PatchNumber = "1.7.0" });
            patches.Add(new Patch { Date = new DateTime(2005, 9, 22), PatchNumber = "1.7.1" });
            patches.Add(new Patch { Date = new DateTime(2005, 10, 10), PatchNumber = "1.8.0" });
            patches.Add(new Patch { Date = new DateTime(2005, 10, 25), PatchNumber = "1.8.1" });
            patches.Add(new Patch { Date = new DateTime(2005, 10, 27), PatchNumber = "1.8.2" });
            patches.Add(new Patch { Date = new DateTime(2005, 11, 15), PatchNumber = "1.8.3" });
            patches.Add(new Patch { Date = new DateTime(2005, 12, 5), PatchNumber = "1.8.4" });
            patches.Add(new Patch { Date = new DateTime(2006, 1, 3), PatchNumber = "1.9.0" });
            patches.Add(new Patch { Date = new DateTime(2006, 1, 10), PatchNumber = "1.9.1" });
            patches.Add(new Patch { Date = new DateTime(2006, 1, 12), PatchNumber = "1.9.2" });
            patches.Add(new Patch { Date = new DateTime(2006, 2, 7), PatchNumber = "1.9.3" });
            patches.Add(new Patch { Date = new DateTime(2006, 2, 14), PatchNumber = "1.9.4" });
            patches.Add(new Patch { Date = new DateTime(2006, 3, 28), PatchNumber = "1.10.0" });
            patches.Add(new Patch { Date = new DateTime(2006, 4, 11), PatchNumber = "1.10.1" });
            patches.Add(new Patch { Date = new DateTime(2006, 5, 2), PatchNumber = "1.10.2" });
            patches.Add(new Patch { Date = new DateTime(2006, 6, 20), PatchNumber = "1.11.0" });
            patches.Add(new Patch { Date = new DateTime(2006, 6, 28), PatchNumber = "1.11.1" });
            patches.Add(new Patch { Date = new DateTime(2006, 7, 11), PatchNumber = "1.11.2" });
            patches.Add(new Patch { Date = new DateTime(2006, 8, 22), PatchNumber = "1.12.0" });
            patches.Add(new Patch { Date = new DateTime(2006, 9, 26), PatchNumber = "1.12.1" });
            patches.Add(new Patch { Date = new DateTime(2006, 10, 13), PatchNumber = "1.12.2" });
            patches.Add(new Patch { Date = new DateTime(2006, 12, 5), PatchNumber = "2.0.1" });
            patches.Add(new Patch { Date = new DateTime(2007, 1, 9), PatchNumber = "2.0.3" });
            patches.Add(new Patch { Date = new DateTime(2007, 1, 12), PatchNumber = "2.0.4" });
            patches.Add(new Patch { Date = new DateTime(2007, 1, 14), PatchNumber = "2.0.5" });
            patches.Add(new Patch { Date = new DateTime(2007, 1, 23), PatchNumber = "2.0.6" });
            patches.Add(new Patch { Date = new DateTime(2007, 2, 13), PatchNumber = "2.0.7" });
            patches.Add(new Patch { Date = new DateTime(2007, 2, 16), PatchNumber = "2.0.8" });
            patches.Add(new Patch { Date = new DateTime(2007, 3, 6), PatchNumber = "2.0.10" });
            patches.Add(new Patch { Date = new DateTime(2007, 4, 3), PatchNumber = "2.0.12" });
            patches.Add(new Patch { Date = new DateTime(2007, 5, 15), PatchNumber = "2.1.0" });
            patches.Add(new Patch { Date = new DateTime(2007, 6, 5), PatchNumber = "2.1.1" });
            patches.Add(new Patch { Date = new DateTime(2007, 6, 19), PatchNumber = "2.1.2" });
            patches.Add(new Patch { Date = new DateTime(2007, 7, 10), PatchNumber = "2.1.3" });
            patches.Add(new Patch { Date = new DateTime(2007, 9, 25), PatchNumber = "2.2.0" });
            patches.Add(new Patch { Date = new DateTime(2007, 10, 2), PatchNumber = "2.2.2" });
            patches.Add(new Patch { Date = new DateTime(2007, 10, 9), PatchNumber = "2.2.3" });
            patches.Add(new Patch { Date = new DateTime(2007, 11, 13), PatchNumber = "2.3.0" });
            patches.Add(new Patch { Date = new DateTime(2008, 1, 8), PatchNumber = "2.3.2" });
            patches.Add(new Patch { Date = new DateTime(2008, 1, 22), PatchNumber = "2.3.3" });
            patches.Add(new Patch { Date = new DateTime(2008, 3, 25), PatchNumber = "2.4.0" });
            patches.Add(new Patch { Date = new DateTime(2008, 4, 1), PatchNumber = "2.4.1" });
            patches.Add(new Patch { Date = new DateTime(2008, 5, 13), PatchNumber = "2.4.2" });
            patches.Add(new Patch { Date = new DateTime(2008, 7, 15), PatchNumber = "2.4.3" });
            patches.Add(new Patch { Date = new DateTime(2008, 10, 14), PatchNumber = "3.0.2" });
            patches.Add(new Patch { Date = new DateTime(2008, 11, 4), PatchNumber = "3.0.3" });
            patches.Add(new Patch { Date = new DateTime(2009, 1, 20), PatchNumber = "3.0.8" });
            patches.Add(new Patch { Date = new DateTime(2009, 1, 27), PatchNumber = "3.0.8a" });
            patches.Add(new Patch { Date = new DateTime(2009, 2, 10), PatchNumber = "3.0.9" });
            patches.Add(new Patch { Date = new DateTime(2009, 4, 14), PatchNumber = "3.1.0" });
            patches.Add(new Patch { Date = new DateTime(2009, 4, 21), PatchNumber = "3.1.1" });
            patches.Add(new Patch { Date = new DateTime(2009, 4, 28), PatchNumber = "3.1.1a" });
            patches.Add(new Patch { Date = new DateTime(2009, 5, 19), PatchNumber = "3.1.2" });
            patches.Add(new Patch { Date = new DateTime(2009, 5, 2), PatchNumber = "3.1.3" });
            patches.Add(new Patch { Date = new DateTime(2009, 8, 4), PatchNumber = "3.2.0" });
            patches.Add(new Patch { Date = new DateTime(2009, 8, 19), PatchNumber = "3.2.0a" });
            patches.Add(new Patch { Date = new DateTime(2009, 9, 22), PatchNumber = "3.2.2" });
            patches.Add(new Patch { Date = new DateTime(2009, 9, 25), PatchNumber = "3.2.2" });
            patches.Add(new Patch { Date = new DateTime(2009, 12, 8), PatchNumber = "3.3.0" });
            patches.Add(new Patch { Date = new DateTime(2009, 12, 14), PatchNumber = "3.3.0a" });
            patches.Add(new Patch { Date = new DateTime(2010, 2, 2), PatchNumber = "3.3.2" });
            patches.Add(new Patch { Date = new DateTime(2010, 3, 23), PatchNumber = "3.3.3" });
            patches.Add(new Patch { Date = new DateTime(2010, 3, 26), PatchNumber = "3.3.3a" });
            patches.Add(new Patch { Date = new DateTime(2010, 5, 22), PatchNumber = "3.3.5" });
            patches.Add(new Patch { Date = new DateTime(2010, 5, 29), PatchNumber = "3.3.5" });
            patches.Add(new Patch { Date = new DateTime(2010, 10, 12), PatchNumber = "4.0.1 or above" });
        }

        public void SetPatch(DateTime dt)
        {
            if (dt <= new DateTime(2007, 01, 17))
                Expansion = "Classic";
            else if (dt <= new DateTime(2008, 11, 14))
                Expansion = "The Burning Crusade";
            else if (dt <= new DateTime(2010, 12, 7))
                Expansion = "Wrath of the Lich King";
            else if (dt <= new DateTime(2010, 9, 25))
                Expansion = "Cataclysm";
            else if (dt != DateTime.MaxValue) //max valus is the default for invalid value, don't report MoP under that circumstance
                Expansion = "Mists of Panderia";

            foreach (Patch patch in patches)
            {
                if (dt >= patch.Date)
                    Patch = patch.PatchNumber;
            }
        }
    }

    public class Patch
    {
        public DateTime Date { get; set; }
        public string PatchNumber { get; set; }
    }
}

﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Input;

namespace WoWExpansionGuide
{
    class Model : INotifyPropertyChanged
    {
        private readonly Patches p = new Patches();

        private string searchDate;
        public string SearchDate { get { return searchDate; }
            set
            {
                searchDate = value;
                OnPropertyChanged("SearchDate");

                p.SetPatch(ValidateTime(value));
                Expansion = p.Expansion;
                Patch = p.Patch;
            } }

        private string expansion;
        public string Expansion { get { return expansion; } set { expansion = value; OnPropertyChanged("Expansion"); } }

        private string patch;

        public string Patch
        {
            get
            {
                if(!String.IsNullOrEmpty(patch))
                    return "Patch " + patch;
                return patch;
            }
            set { patch = value; OnPropertyChanged("Patch"); }
        }

        private DateTime ValidateTime(string date)
        {
            DateTime dt;
            return DateTime.TryParse(date, out dt) ? dt : DateTime.MaxValue;
        }
        
        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propName));
        }
    }

    /// <summary>
    /// A command whose sole purpose is to 
    /// relay its functionality to other
    /// objects by invoking delegates. The
    /// default return value for the CanExecute
    /// method is 'true'.
    /// </summary>
    public class RelayCommand : ICommand
    {
        #region Fields

        private readonly Action<object> _execute;
        private readonly Predicate<object> _canExecute;

        #endregion // Fields

        #region Constructors

        /// <summary>
        /// Creates a new command that can always execute.
        /// </summary>
        /// <param name="execute">The execution logic.</param>
        public RelayCommand(Action<object> execute)
            : this(execute, null)
        {
        }

        /// <summary>
        /// Creates a new command.
        /// </summary>
        /// <param name="execute">The execution logic.</param>
        /// <param name="canExecute">The execution status logic.</param>
        public RelayCommand(Action<object> execute, Predicate<object> canExecute)
        {
            if (execute == null)
                throw new ArgumentNullException("execute");

            _execute = execute;
            _canExecute = canExecute;
        }

        #endregion // Constructors

        #region ICommand Members

        [DebuggerStepThrough]
        public bool CanExecute(object parameters)
        {
            return _canExecute == null ? true : _canExecute(parameters);
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public void Execute(object parameters)
        {
            _execute(parameters);
        }

        #endregion // ICommand Members
    }
}
